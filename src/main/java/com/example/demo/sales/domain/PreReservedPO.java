package com.example.demo.sales.domain;

import com.example.demo.common.domain.BusinessPeriod;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Getter
@NoArgsConstructor(force = true, access = AccessLevel.PROTECTED)
public class PreReservedPO {
    @Id
    @GeneratedValue
    Long id;

    @OneToOne
    TentativeReservation tentativeReservation;

    LocalDate issueDate;
    LocalDate paymentSchedule;

    @Column(precision = 8, scale = 2)
    BigDecimal total;

    @Enumerated(EnumType.STRING)
    POStatus status;

    @Embedded
    BusinessPeriod rentalPeriod;

    public static PreReservedPO of(TentativeReservation tentativeReservation, BusinessPeriod period) {
        PreReservedPO preReservedPO = new PreReservedPO();
        preReservedPO.tentativeReservation = tentativeReservation;
        preReservedPO.rentalPeriod = period;

        preReservedPO.issueDate = LocalDate.now();
        preReservedPO.status = POStatus.PENDING;
        return preReservedPO;
    }

    public void setStatus(POStatus newStatus) {
        status = newStatus;
    }
}

