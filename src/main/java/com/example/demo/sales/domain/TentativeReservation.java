package com.example.demo.sales.domain;

import com.example.demo.common.domain.BusinessPeriod;
import com.example.demo.inventory.domain.model.PlantInventoryEntry;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class TentativeReservation {

    @Id
    Long id;

    @Embedded
    BusinessPeriod rentalPeriod;

    @ManyToOne
    PlantInventoryEntry plant;

    @Enumerated
    String status;
}
