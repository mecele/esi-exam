package com.example.demo.sales.repository;

import com.example.demo.sales.domain.PreReservedPO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PreReservedPORepository extends JpaRepository<PreReservedPO, Long> {
}
