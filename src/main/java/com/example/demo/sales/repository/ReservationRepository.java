package com.example.demo.sales.repository;

import com.example.demo.sales.domain.TentativeReservation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ReservationRepository extends JpaRepository<TentativeReservation, Long> {
}
