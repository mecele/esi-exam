package com.example.demo.sales.rest;

import com.example.demo.common.application.exception.PlantNotFoundException;
import com.example.demo.inventory.application.service.InventoryService;
import com.example.demo.sales.application.dto.ReservationDTO;
import com.example.demo.sales.application.service.ReservationService;
import com.example.demo.sales.application.service.SalesService;
import com.example.demo.sales.domain.PreReservedPO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;

@RestController
@RequestMapping("/api/reservations")
public class ReservationController {

    @Autowired
    ReservationService reservationService;

    @Autowired
    InventoryService inventoryService;

    @Autowired
    SalesService salesService;

    @PostMapping
    public ResponseEntity<ReservationDTO> createTentativeReservation(@RequestBody ReservationDTO reservationDTO) throws Exception {
        ReservationDTO newlyCreatedTentativeReservation = reservationService.createReservation(reservationDTO);

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(new URI(newlyCreatedTentativeReservation.getId().getHref()));
        // The above line won't working until you update PurchaseOrderDTO to extend ResourceSupport

        return new ResponseEntity<>(newlyCreatedTentativeReservation, headers, HttpStatus.CREATED);
    }

    @PostMapping("/{id}")
    public ResponseEntity<ReservationDTO> acceptTentativeReservation(@PathVariable Long id) throws Exception {
        ReservationDTO tentativeReservation = reservationService.acceptTentativeReservation(id);

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(new URI(tentativeReservation.getId().getHref()));
        // The above line won't working until you update PurchaseOrderDTO to extend ResourceSupport

        return new ResponseEntity<>(tentativeReservation, headers, HttpStatus.CREATED);
    }

    @PostMapping("/po")
    public ResponseEntity<PreReservedPO> createPreReservedPO(@RequestBody PreReservedPO preReservedPO) throws Exception {
        PreReservedPO newlyCreatedPO = reservationService.createPreReservedPO(preReservedPO);
        // The above line won't working until you update PurchaseOrderDTO to extend ResourceSupport

        return new ResponseEntity<>(newlyCreatedPO, HttpStatus.CREATED);
    }

    @PostMapping("{id}/po/accept")
    public PreReservedPO acceptPurchaseOrder(@PathVariable Long id) throws Exception {
        try {
            return reservationService.acceptPreReservedPO(id);
        } catch (Exception ex) {
            // Add code to Handle Exception (Change return null with the solution)
            return null;
        }
    }

    @DeleteMapping("{id}/po/reject")
    public PreReservedPO rejectPurchaseOrder(@PathVariable Long id) throws Exception {
        try {
            return reservationService.rejectPreReservedPO(id);
        } catch (Exception ex) {
            // Add code to Handle Exception (Change return null with the solution)
            return null;
        }
    }




    @ExceptionHandler(PlantNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public void handPlantNotFoundException(PlantNotFoundException ex) {
    }
}
