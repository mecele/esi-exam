package com.example.demo.sales.application.dto;

import com.example.demo.common.application.dto.BusinessPeriodDTO;
import com.example.demo.common.rest.ResourceSupport;
import com.example.demo.inventory.application.dto.PlantInventoryEntryDTO;
import lombok.Data;

@Data
public class ReservationDTO extends ResourceSupport {
    Long _id;
    BusinessPeriodDTO rentalPeriod;
    PlantInventoryEntryDTO plant;
    String status;
}
