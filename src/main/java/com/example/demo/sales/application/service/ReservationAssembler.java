package com.example.demo.sales.application.service;

import com.example.demo.common.application.dto.BusinessPeriodDTO;
import com.example.demo.inventory.application.service.PlantInventoryEntryAssembler;
import com.example.demo.sales.application.dto.PurchaseOrderDTO;
import com.example.demo.sales.application.dto.ReservationDTO;
import com.example.demo.sales.domain.PurchaseOrder;
import com.example.demo.sales.domain.TentativeReservation;
import com.example.demo.sales.rest.ReservationController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

public class ReservationAssembler  extends ResourceAssemblerSupport<TentativeReservation, ReservationDTO> {

    @Autowired
    PlantInventoryEntryAssembler plantInventoryEntryAssembler;

    public ReservationAssembler() {
        super(ReservationController.class, ReservationDTO.class);
    }

    @Override
    public ReservationDTO toResource(TentativeReservation reservation) {
        ReservationDTO dto = createResourceWithId(reservation.getId(), reservation);

        dto.set_id(reservation.getId());
        dto.setRentalPeriod(BusinessPeriodDTO.of(reservation.getRentalPeriod().getStartDate(), reservation.getRentalPeriod().getEndDate()));
        dto.setPlant(plantInventoryEntryAssembler.toResource(reservation.getPlant()));

        return dto;
    }
}
