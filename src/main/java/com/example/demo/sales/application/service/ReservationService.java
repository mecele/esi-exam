package com.example.demo.sales.application.service;

import com.example.demo.common.application.service.BusinessPeriodValidator;
import com.example.demo.common.domain.BusinessPeriod;
import com.example.demo.inventory.domain.model.PlantInventoryEntry;
import com.example.demo.inventory.domain.model.PlantInventoryItem;
import com.example.demo.inventory.domain.model.PlantReservation;
import com.example.demo.inventory.domain.repository.InventoryRepository;
import com.example.demo.inventory.domain.repository.PlantInventoryEntryRepository;
import com.example.demo.inventory.domain.repository.PlantReservationRepository;
import com.example.demo.sales.application.dto.ReservationDTO;
import com.example.demo.sales.domain.POStatus;
import com.example.demo.sales.domain.PreReservedPO;
import com.example.demo.sales.domain.TentativeReservation;
import com.example.demo.sales.repository.PreReservedPORepository;
import com.example.demo.sales.repository.ReservationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.DataBinder;

import java.util.List;

import static java.time.temporal.ChronoUnit.DAYS;


@Service
@SuppressWarnings("ALL")
public class ReservationService {

    @Autowired
    ReservationRepository reservationRepository;

    @Autowired
    PlantInventoryEntryRepository plantInventoryEntryRepository;

    @Autowired
    InventoryRepository inventoryRepository;

    @Autowired
    PlantReservationRepository plantReservationRepository;

    @Autowired
    ReservationAssembler reservationAssembler;

    @Autowired
    PreReservedPORepository preReservedPORepository;

    public ReservationDTO createReservation(ReservationDTO reservationDTO) throws Exception {

        BusinessPeriod period = BusinessPeriod.of(
                reservationDTO.getRentalPeriod().getStartDate(),
                reservationDTO.getRentalPeriod().getEndDate());

        DataBinder binder = new DataBinder(period);
        binder.addValidators(new BusinessPeriodValidator());
        binder.validate();

        if (binder.getBindingResult().hasErrors())
            throw new Exception("Invalid Reservation Period");

        if (reservationDTO.getPlant() == null)
            throw new Exception("Invalid Input Plant");

        PlantInventoryEntry plant = plantInventoryEntryRepository.findById(reservationDTO.getPlant().get_id()).orElse(null);

        if (plant == null)
            throw new Exception("Plant NOT Found");

        if (DAYS.between(reservationDTO.getRentalPeriod().getStartDate(), reservationDTO.getRentalPeriod().getEndDate()) < 5) {
            reservationDTO.setStatus("REJECTED");
            throw new Exception("Business Period need to be greater or equal to 5 days");
        }

        TentativeReservation tentativeReservation = new TentativeReservation();
        tentativeReservation.setStatus("PENDING");


        List<PlantInventoryItem> availableItems = inventoryRepository.findAvailableItems(
                reservationDTO.getPlant().get_id(),
                reservationDTO.getRentalPeriod().getStartDate(),
                reservationDTO.getRentalPeriod().getEndDate());

        if (availableItems.size() == 0) {
            reservationDTO.setStatus("REJECTED");
            throw new Exception("No available items");
        }

        PlantReservation reservation = new PlantReservation(); // Tentative reservation is a also a reservation so tentative reservation also occupies the plant inventory item.
        reservation.setSchedule(tentativeReservation.getRentalPeriod());
        reservation.setPlant(availableItems.get(0));

        plantReservationRepository.save(reservation);

        reservationRepository.save(tentativeReservation);
        return reservationAssembler.toResource(tentativeReservation);

    }

    public ReservationDTO acceptTentativeReservation(Long id) throws Exception {
        TentativeReservation tentativeReservation = getTentativeReservation(id);
        getTentativeReservation(id).setStatus("ACCEPTED");
        reservationRepository.save(tentativeReservation);
        return reservationAssembler.toResource(tentativeReservation);
    }


    public TentativeReservation getTentativeReservation(Long id) throws Exception {
        return reservationRepository.getOne(id);
    }

    public PreReservedPO createPreReservedPO(PreReservedPO preReservedPO) throws Exception {

        TentativeReservation tentativeReservation = reservationRepository.findById(preReservedPO.getId()).orElse(null);

        if (tentativeReservation == null)
            throw new Exception("Tentative reservation NOT Found");

        preReservedPO.setStatus(POStatus.PENDING);

        return preReservedPORepository.save(preReservedPO);

    }

    public PreReservedPO acceptPreReservedPO(Long id) throws Exception {
        PreReservedPO preReservedPO = preReservedPORepository.findById(id).orElse(null);

        if (preReservedPO == null)
            throw new Exception("PO NOT Found");
        preReservedPO.setStatus(POStatus.OPEN);
        return preReservedPORepository.save(preReservedPO);

    }

    public PreReservedPO rejectPreReservedPO(Long id) throws Exception {

        PreReservedPO preReservedPO = preReservedPORepository.findById(id).orElse(null);

        if (preReservedPO == null)
            throw new Exception("PO NOT Found");

        preReservedPO.setStatus(POStatus.REJECTED);

        return preReservedPORepository.save(preReservedPO);

    }


}
